package com.nifa.bootpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootpocApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootpocApplication.class, args);
	}

}
