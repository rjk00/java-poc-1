# java-poc
Proof Of Concept in Java/Spring Boot

To run application:

1. In the application root directory, run build.sh.
2. Once build.sh has completed, verify that the mysql and java-poc_spring-app containers are running.
3. To create the database tables and seed data, run load_data.sh from the application root directory.
