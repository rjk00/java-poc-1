export default {
  url: 'https://dev-956783.okta.com',
  issuer: 'https://dev-956783.okta.com/oauth2/default',
  redirectUri: window.location.origin + '/implicit/callback',
  clientId: '0oa1b4uemt9JB7idE357',
  API_URL: window.location.protocol + '//' + window.location.hostname + ':' + process.env.REACT_APP_API_PORT
};
